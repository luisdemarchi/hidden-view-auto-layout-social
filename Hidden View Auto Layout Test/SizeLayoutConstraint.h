//
//  SizeLayoutConstraint.h
//  Hidden View Auto Layout Test
//
//  Created by Luis Antonio De Marchi on 18/08/14.
//  Copyright (c) 2014 Daniel Tull. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SizeLayoutConstraint : NSLayoutConstraint
@property (nonatomic) BOOL collapsed;

@end
