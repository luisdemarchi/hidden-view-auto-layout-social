//
//  ViewController.m
//  Hidden View Auto Layout Test
//
//  Created by Daniel Tull on 14/03/2014.
//  Copyright (c) 2014 Daniel Tull. All rights reserved.
//

#import "ViewController.h"
#import "UIView+HideConstraints.h"

@interface ViewController (){
    BOOL i1, i2, i3;
}
@property (weak, nonatomic) IBOutlet UILabel *label1;
@property (weak, nonatomic) IBOutlet UILabel *label2;
@property (weak, nonatomic) IBOutlet UILabel *label3;

@property (weak, nonatomic) IBOutlet UIView *view1;
@property (weak, nonatomic) IBOutlet UIView *view2;
@property (weak, nonatomic) IBOutlet UIView *view3;
@end

@implementation ViewController

- (void)viewDidLoad {
	[super viewDidLoad];
	self.label1.text = @"1. Label";
	self.label2.text = @"2. Label";
	self.label3.text = @"3. Label";
}

- (IBAction)toggelText1:(id)sender {
    i1 = !i1;
    [self hiddenView:_label1 hidden:i1];
    [self hiddenView:_view1 hidden:i1];
}
- (IBAction)toggleText2:(id)sender {
    i2 = !i2;
    [self hiddenView:_label2 hidden:i2];
    [self hiddenView:_view2 hidden:i2];
}
- (IBAction)toggleText3:(id)sender {
    i3 = !i3;
    [self hiddenView:_label3 hidden:i3];
    [self hiddenView:_view3 hidden:i3];
}

-(void) hiddenView:(UIView*)view hidden:(BOOL)hidden{
    if (hidden) {
		view.dct_collapsed = YES;
	} else {
		view.dct_collapsed = NO;
	}
}


@end
