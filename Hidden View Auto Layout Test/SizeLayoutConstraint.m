//
//  SizeLayoutConstraint.m
//  Hidden View Auto Layout Test
//
//  Created by Luis Antonio De Marchi on 18/08/14.
//  Copyright (c) 2014 Daniel Tull. All rights reserved.
//

#import "SizeLayoutConstraint.h"


@interface SizeLayoutConstraint ()
@property (nonatomic) CGFloat originalConstant;
@end

@implementation SizeLayoutConstraint

- (void)awakeFromNib {
	[super awakeFromNib];
	self.originalConstant = self.constant;
}

- (void)setCollapsed:(BOOL)collapsed {
	_collapsed = collapsed;
	self.constant = _collapsed ? 0.0f : self.originalConstant;
}
@end